sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("spar.create.bp.BP_Create.controller.home", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf spar.create.bp.BP_Create.view.home
		 */
		onInit: function () {

		},

		onSubmit: function (oEvent) {
			var xcsrfToken = "";
			var wfParameter = {};

			wfParameter = {
				"BusinessPartner": "82128",
				"Customer": "",
				"Supplier": "",
				"AcademicTitle": "",
				"AuthorizationGroup": "",
				"BusinessPartnerCategory": "2",
				"BusinessPartnerFullName": "BLT SUPERSPAR",
				"BusinessPartnerGrouping": "ZSTV",
				"BusinessPartnerName": "BLT SUPERSPAR",
				"BusinessPartnerUUID": "00505686-6cf6-1eda-93cd-5b4587a480c3",
				"CorrespondenceLanguage": "",
				"CreatedByUser": "SINGHVIJ",
				"CreationDate": "\/Date(1581552000000)\/",
				"CreationTime": "PT16H15M36S",
				"FirstName": "",
				"FormOfAddress": "",
				"Industry": "",
				"InternationalLocationNumber1": "6001008",
				"InternationalLocationNumber2": "40000",
				"IsFemale": false,
				"IsMale": false,
				"IsNaturalPerson": "",
				"IsSexUnknown": false,
				"Language": "",
				"LastChangeDate": null,
				"LastChangeTime": "PT00H00M00S",
				"LastChangedByUser": "",
				"LastName": "",
				"LegalForm": "",
				"OrganizationBPName1": "BLT-82128",
				"OrganizationBPName2": "",
				"OrganizationBPName3": "",
				"OrganizationBPName4": "",
				"OrganizationFoundationDate": null,
				"OrganizationLiquidationDate": null,
				"SearchTerm1": "ALPHA",
				"AdditionalLastName": "",
				"BirthDate": null,
				"BusinessPartnerIsBlocked": false,
				"BusinessPartnerType": "",
				"ETag": "SINGHVIJ20200213161536",
				"GroupBusinessPartnerName1": "",
				"GroupBusinessPartnerName2": "",
				"IndependentAddressID": "",
				"InternationalLocationNumber3": "0",
				"MiddleName": "",
				"NameCountry": "",
				"NameFormat": "",
				"PersonFullName": "",
				"PersonNumber": "",
				"IsMarkedForArchiving": false
			};

			$.ajax({
				url: "/FS1/API_BUSINESS_PARTNER?$format=json",
				type: "GET",
				headers: {
					"X-CSRF-Token": "Fetch"
				},
				success: function (result, xhr, data) {
					xcsrfToken = data.getResponseHeader("X-CSRF-Token");
					$.ajax({
						url: "/FS1/API_BUSINESS_PARTNER/A_BusinessPartner",
						method: "POST",
						headers: {
							"X-CSRF-Token": xcsrfToken,
							"Content-Type": "application/json"
						},
						data: JSON.stringify(wfParameter),
						success: function (result, xhr, data) {
							sap.m.MessageToast.show("Work Flow started successfuly");
						},
						error: function (jqXHR, status, error) {
							sap.m.MessageToast.show("Filed to start the workflow");
						}
					});
				},
				error: function (jqXHR, status, error) {
					sap.m.MessageToast.show("Filed to fetch the XSCRF Token");
				}
			});
			// debugger;
		}
	});

});