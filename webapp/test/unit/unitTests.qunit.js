/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"spar/create/bp/BP_Create/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});